#!/usr/bin/env python3

import tensorflow as tf
from keras.layers import Input, Flatten, Dense, Dropout
from keras.models import Model


def main():
    # Load and prepare the MNIST dataset and convert the samples
    # from integers to floating-point numbers
    print('\n### Download and prepare MNIST\n')
    mnist = tf.keras.datasets.mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0

    # Build the keras model using the functional API
    inputs = Input(shape=x_train.shape[1:])
    x = Flatten()(inputs)
    x = Dense(512, activation=tf.nn.relu)(x)
    x = Dropout(0.2)(x)
    predictions = Dense(10, activation=tf.nn.softmax)(x)

    model = Model(inputs=inputs, outputs=predictions)
    model.compile(
        optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy']
    )

    # Train and evaluate the model loss and accuracy
    print('\n### Train model\n')
    model.fit(x_train, y_train, epochs=5)
    # Save the full model architecture, weights, and optimizer state
    model.save('tf_example_model.h5')
    print('\n### Evaluate model\n')
    performance = model.evaluate(x_test, y_test)
    print(f'\nmodel loss: {performance[0]}\nmodel accuracy: {performance[1]}\n')


if __name__ == '__main__':
    main()
